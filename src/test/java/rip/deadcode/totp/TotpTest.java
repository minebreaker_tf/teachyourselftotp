package rip.deadcode.totp;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

import static com.google.common.truth.Truth.assertThat;

class TotpTest {

    @Test
    void testHotp() {
        byte[] key = "12345678901234567890".getBytes( StandardCharsets.UTF_8 );
        String result = Totp.hotp( key, 0, 6 );
        assertThat( result ).isEqualTo( "755224" );
        result = Totp.hotp( key, 1, 6 );
        assertThat( result ).isEqualTo( "287082" );
        result = Totp.hotp( key, 2, 6 );
        assertThat( result ).isEqualTo( "359152" );
        result = Totp.hotp( key, 3, 6 );
        assertThat( result ).isEqualTo( "969429" );
        result = Totp.hotp( key, 4, 6 );
        assertThat( result ).isEqualTo( "338314" );
        result = Totp.hotp( key, 5, 6 );
        assertThat( result ).isEqualTo( "254676" );
        result = Totp.hotp( key, 6, 6 );
        assertThat( result ).isEqualTo( "287922" );
        result = Totp.hotp( key, 7, 6 );
        assertThat( result ).isEqualTo( "162583" );
        result = Totp.hotp( key, 8, 6 );
        assertThat( result ).isEqualTo( "399871" );
        result = Totp.hotp( key, 9, 6 );
        assertThat( result ).isEqualTo( "520489" );
    }

    @Test
    void testTotp() {
        byte[] key = "12345678901234567890".getBytes( StandardCharsets.UTF_8 );
        String result = Totp.totp( key, 59, 30, 8 );
        assertThat( result ).isEqualTo( "94287082" );
        result = Totp.totp( key, 1111111109, 30, 8 );
        assertThat( result ).isEqualTo( "07081804" );
        result = Totp.totp( key, 1111111111, 30, 8 );
        assertThat( result ).isEqualTo( "14050471" );
        result = Totp.totp( key, 1234567890, 30, 8 );
        assertThat( result ).isEqualTo( "89005924" );
        result = Totp.totp( key, 2000000000, 30, 8 );
        assertThat( result ).isEqualTo( "69279037" );
    }
}
