package rip.deadcode.totp;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.StringJoiner;

public final class Totp {

    // https://tools.ietf.org/html/rfc4226
    public static String hotp( byte[] secret, long count, int digit ) {

        byte[] hash = Hashing.hmacSha1( secret )
                             .hashLong( Long.reverseBytes( count ) )
                             .asBytes();

        int offset = hash[19] & 0x0F;
        int code = ( hash[offset] & 0x7f ) << 24 |
                   ( hash[offset + 1] & 0xff ) << 16 |
                   ( hash[offset + 2] & 0xff ) << 8 |
                   ( hash[offset + 3] & 0xff );

        var codeAsStr = Integer.toString( code );
        return codeAsStr.substring( codeAsStr.length() - digit );
    }

    // https://tools.ietf.org/html/rfc6238
    public static String totp( byte[] secret, long time, int step, int digit ) {
        long timeCounter = time / step;
        return hotp( secret, timeCounter, digit );
    }

    // https://github.com/google/google-authenticator/wiki/Key-Uri-Format
    public static URI toUri( String label, String secret, String issuer ) {

        var query = new StringJoiner( "&" );
        query.add( "secret=" + BaseEncoding.base32().omitPadding().encode( secret.getBytes( StandardCharsets.UTF_8 ) ) );
        query.add( "issuer=" + issuer );

        try {
            return new URI( "otpauth", "totp", "/" + label, query.toString(), null );
        } catch ( URISyntaxException e ) {
            throw new RuntimeException( e );
        }
    }

    public static void main( String[] args ) {
        String key = "put your key here.";
        System.out.println( totp(
                BaseEncoding.base32().omitPadding().decode( key.toUpperCase() ),
                System.currentTimeMillis() / 1000,
                30,
                6
        ) );
    }
}
